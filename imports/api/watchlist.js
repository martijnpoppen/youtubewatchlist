import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import { check } from 'meteor/check';

/**
 * Set a new mongo db. and export it so its available over the whole app
 */
export const Watchlist = new Mongo.Collection('watchlist');

if (Meteor.isServer) {
  /**
   * Publish an event on the server side. this will publish all the changes to the client
   */
   Meteor.publish('watchlist', function watchlistPublication() {
    return Watchlist.find();
  });
}

Meteor.methods({
  /**
   * this function inserts the new videos into the watchlist
   */
  'watchlist.insert'(text, videoId) {
    check(text, String);

    Watchlist.insert({
      text,
      videoId,
      createdAt: new Date(),
      duration: 0,
      startedAt: null
    });
  },

  /**
   * this function removes items from the watchlist
   */
  'watchlist.remove'(taskId) {
    check(taskId, String);

    Watchlist.remove(taskId);
  },

  /**
   * this function updates the time in the db so the client know where the video is
   */
  'watchlist.update'(taskId, duration, startedAt) {
    check(taskId, String);

    const list = Watchlist.findOne(taskId);

    Watchlist.update(taskId, { $set: { duration: duration, startedAt: startedAt } });
  },
});
