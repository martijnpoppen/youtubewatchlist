import { Meteor } from 'meteor/meteor';
import { check } from 'meteor/check';
import { ReactiveDict } from 'meteor/reactive-dict';

/**
 * set a new reaactive dictionary for the search results and export
 */
var results = new ReactiveDict();
results.set('searchResults', [] );

const key = 'AIzaSyDY6w00ldSHnA92a4Y9SeG_9cCaczG52d0';

Meteor.methods({

  /**
   * this function calls the youtube search function and search for videos with 
   * specified text variable
   * then call the youtube.http to make the GET call
   */
  'youtube.search'(text) {
    check(text, String);

    var url = "https://www.googleapis.com/youtube/v3/search";
    var params = {
        key: key,
        q: text,
        part: "snippet",
        maxResults: 12,
        safeSearch: "strict",
        dataType: "json",
        regionCode: "NL",
        type: "video"
    };

    Meteor.call('youtube.http', url, params);
  },

  /**
   * Get a single video from the youtube APi and return it
   */
  'youtube.getVideo'(id) {
    check(id, String);

    var url = "https://www.googleapis.com/youtube/v3/videos?part=contentDetails";
    var params = {
        id: id,
        type: "video",
        key: key
    };

    return HTTP.call( 'GET', url, {params: params});

  },

  /**
   * A get call to return anything from an API with url and params
   * The results are returned in the reactive dictionary , so the client receives the update.
   */
  'youtube.http'(url, params) {
    HTTP.call( 'GET', url, {params: params}, function( error, response ) {
      if ( error ) {
        console.log( error );
      } else {
        results.set('searchResults', response.data.items);
      }
    });
  },
});

/**
 * export the reactive dictionary so its availble on the client
 */
export {results as Youtube};
