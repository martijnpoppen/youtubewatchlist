import { Meteor } from 'meteor/meteor';
import { ReactiveDict } from 'meteor/reactive-dict';
import { Youtube} from './youtube.js';
import { Watchlist } from './watchlist.js';

var watchlistVideo, playerStopped, interval, timer;
timer = new ReactiveDict();
timer.set('Timer', null );

if(Meteor.isServer) {
  Meteor.startup(() => {
    /**
     * when server started, call the player in the player API
     */
     Meteor.call('player.observe');

     Meteor.call('player.start');  
  });

  
  Meteor.methods({
    /**
     * observe the watchlist DB to see if theres an added value
     * @param (Variabe) playerStopped. this indicates if the player i running or stopped.
     * the observe function checks if the value is added, if so checks if the player is stopped
     * then the player will be restarted
     * when the player is running and the current playing video is removed stop the player
     */
    'player.observe'() {
      playerStopped = false;

      Watchlist.find().observe({
        added: function(listitem) {
         if(playerStopped) {
          Meteor.call('player.start');
         }
        },
      });
    },

    /**
     * when server is started this function is called and this call the getvideo function
     * when there's no video set the Playerstopped to true. playerStopped is checked by the observe function
     */
      'player.start'() {
        watchlistVideo = Watchlist.find({}, {sort: { createdAt: 1 }, limit: 1 }).fetch();

        if(watchlistVideo.length) {
          console.log('started '+watchlistVideo[0].text);
          playerStopped = false;
          Meteor.call('player.getvideo', watchlistVideo[0]);
        } else {
          playerStopped = true;
          console.log('waiting');
        }

      },

    /**
     * get the video duration from youtube and convert this to milliseconds and call the timer to start
     */
      'player.getvideo'(watchlistVideo) {
        var ytVideo, duration, convertedTime;

        ytVideo = Meteor.call('youtube.getVideo', watchlistVideo.videoId);
        duration = ytVideo.data.items[0].contentDetails.duration;

        convertedTime = Meteor.call('player.convertTime', duration);

        Meteor.call('player.timerStart', convertedTime, watchlistVideo)

      },

      /**
       * moment.js converts the duration asmilliseconds
       */
      'player.convertTime'(time) {

         return moment.duration(time).asMilliseconds();

      },

      /**
       * when this is called the servers start a timer to check if the duration of the video
       * is reached and then removes the watchlist item.
       */
      'player.timerStart'(duration, watchlistVideo) {
        var ms, i;
        
        ms = 500;
        i = 0
        Meteor.call('watchlist.update', watchlistVideo._id, duration, new Date());

        interval = Meteor.setInterval(function(){
          i+=ms;
          timer.set('Timer', i);

          if(i > duration+1000) {        
            Meteor.call('watchlist.remove', watchlistVideo._id);
            Meteor.call('player.stop');
          }
        }, ms);

      },

      'player.stop'(watchlistVideo) {
        Meteor.clearInterval(interval);
        Meteor.call('player.start');
      },

  });
}

Meteor.methods({
  /**
   * Returns reactive dictionary to client
   */
  'player.returnTime'() {
    return timer.get('Timer');
  },
});

