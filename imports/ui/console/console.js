import { Meteor } from 'meteor/meteor';
import { Template } from 'meteor/templating';
import { ReactiveDict } from 'meteor/reactive-dict';
import { Youtube } from '../../api/youtube.js';
import { Watchlist } from '../../api/watchlist.js';
import './results.js';
import './watchlist.js';
import './console.html';


/**
 * Subscribe to watchlist api to get reactive changes in the DB
 */
Template.console.onCreated(function bodyOnCreated() {
  Meteor.subscribe('watchlist');
});


Template.console.helpers({
  
  /**
   * find all watchlist items in DB and return them to the watchlist template
   */
  watchlist() {
    const instance = Template.instance();
    return Watchlist.find({}, { sort: { createdAt: 1 } });
  },

  /**
   * Get searchresults from reactive dictionary in the youtube APi 
   * and return them to the results template
   */
  results() {
    return Youtube.get('searchResults');
  },
});

Template.console.events({

  /**
   * When submitting the search form call the search function in the Youtube api
   * @param {Variable} text contains the search text from the searchbar
   */
   'submit .search-video'(event) {
    event.preventDefault();

    const target = event.target;
    const text = target.text.value;

    Meteor.call('youtube.search', text);
  },

  /**
   * When there are seachresults this function clears the results
   * @param {Object} the reactive dictionary will be set to 0 results
   */
   'click .clear-results'(event) {
      event.preventDefault();
      Youtube.set('searchResults', []);
  },
});