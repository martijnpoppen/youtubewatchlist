import { Meteor } from 'meteor/meteor';
import { Template } from 'meteor/templating';
import './watchlist.html';


Template.list.events({
  /**
   * When cliciking on the x mark this call the remove function in the watchlist API
   * @param {Variable} this._id the id the element has in de DB
   */
  'click .delete'() {
    Meteor.call('watchlist.remove', this._id);
  }, 
});