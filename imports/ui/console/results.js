import { Meteor } from 'meteor/meteor';
import { Template } from 'meteor/templating';
import './results.html';


Template.result.events({
  /**
   * When clicking on the add video this will call the insert to watchlist function
   * in the watchlist API. 
   * @param {Variable} videoId the videoId from youtube
   */
   'click .add-video'(event) {
    event.preventDefault();
    
    const target = this;
    const text = target.snippet.title;
    const videoId = target.id.videoId;

    Meteor.call('watchlist.insert', text, videoId);

  },
});