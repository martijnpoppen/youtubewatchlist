import { Meteor } from 'meteor/meteor';
import { Template } from 'meteor/templating';
import { Watchlist } from '../../api/watchlist.js';
import '../../api/player.js';
import './player.html';


/**
 * Subscribe to watchlist api to get reactive changes in the DB
 */
Template.player.onCreated(function bodyOnCreated() {
  Meteor.subscribe('watchlist');
});

Template.player.helpers({
  /**
   * Get a single watchlist item and return it to the videoplayer template 
   */
  videoplayer() {
    const instance = Template.instance();
    return Watchlist.find({}, {sort: { createdAt: 1 }, limit: 1 });
  },
  /**
   * Get the current videotime from the server 
   */
  timer() {
    return ReactiveMethod.call('player.returnTime');
  }
});